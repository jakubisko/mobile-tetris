import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { TexturePool } from './game/texture-pool';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  texturesLoaded = false;

  constructor(swUpdate: SwUpdate, texturePool: TexturePool) {
    this.checkForUpdates(swUpdate);
    texturePool.preloadTextures(() => this.texturesLoaded = true);
  }

  private checkForUpdates(swUpdate: SwUpdate) {
    swUpdate.available.subscribe(event => {
      if (confirm('There is a newer version of this app available. Do you want to reload?')) {
        swUpdate.activateUpdate().then(() => document.location.reload());
      }
    });
  }

  preventContextMenu() {
    return false;
  }

}
