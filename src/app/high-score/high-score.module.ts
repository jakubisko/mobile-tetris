import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EnterNameComponent } from './enter-name/enter-name.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [EnterNameComponent]
})
export class HighScoreModule { }
