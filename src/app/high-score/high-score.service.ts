import { Injectable } from '@angular/core';
import { HighScores, maxHighScores, currentHighScoresVersion } from './high-score';
import { LocalStorage } from './local-storage';

/**
 * Local storage is shared for all pages of same origin. Two applications deployed on localhost will share local
 * storage. Use prefix to differentiate between them.
 */
const LOCAL_STORAGE_KEY = 'MOBILE_TETRIS_HIGHSCORE';

@Injectable({
  providedIn: 'root'
})
export class HighScoreService {

  private localStorage = new LocalStorage<HighScores>(LOCAL_STORAGE_KEY, currentHighScoresVersion);

  /** Returns whether browser supports saving high score. */
  public isSavingAvailable(): boolean {
    return this.localStorage.isStorageAvailable();
  }

  public getHighScores(): HighScores {
    return this.localStorage.readFromStorage() || this.generateDefaultHighScores();
  }

  private generateDefaultHighScores(): HighScores {
    const highScores: HighScores = { items: [] };
    for (let i = 0; i < maxHighScores; i++) {
      highScores.items.push({
        name: 'Anonymous',
        score: 0
      });
    }
    return highScores;
  }

  public resetHighScores() {
    this.localStorage.delete();
  }

  /** Returns whether given score is enough to be added to high scores. */
  public eligibleForHighScore(score: number): boolean {
    const highScores = this.getHighScores();
    return highScores.items[maxHighScores - 1].score < score;
  }

  /** Adds a new score to high scores. Does nothing if the new high score is not eligible. */
  public addHighScore(name: string, newScore: number) {
    if (!this.eligibleForHighScore(newScore)) {
      return;
    }

    const highScores = this.getHighScores();
    highScores.items.push({
      name: name,
      score: newScore
    });

    highScores.items.sort((x, y) => y.score - x.score);
    highScores.items = highScores.items.slice(0, maxHighScores);

    this.localStorage.writeToStorage(highScores);
  }

}
