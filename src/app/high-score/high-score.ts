
/** Defines how many items are remembered in high scores. */
export const maxHighScores = 3;

/** Increase the version number whenever the class for persisting changes. */
export const currentHighScoresVersion = 2;

export interface HighScores {
  items: HighScoreItem[]; // items[0] is best score
}

export interface HighScoreItem {
  name: string;
  score: number;
}
