import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { maxHighScores } from '../high-score';

@Component({
  templateUrl: './enter-name.component.html',
})
export class EnterNameComponent {

  readonly maxHighScores = maxHighScores;

  newName = '';
  score: number;

  private onSubmit: (newName: string) => void;

  public constructor(private activeModal: NgbActiveModal) { }

  public static openModal(modalService: NgbModal, score: number, onSubmit: (newName: string) => void) {
    const modal = modalService.open(EnterNameComponent, {
      backdrop: 'static', keyboard: false, centered: true
    }).componentInstance as EnterNameComponent;
    modal.onSubmit = onSubmit;
    modal.score = score;
  }

  done() {
    this.activeModal.close();
    this.onSubmit(this.newName);
  }

}
