import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { NewGameComponent } from './new-game/new-game.component';
import { HighScoreModule } from '../high-score/high-score.module';
import { ControlsComponent } from './controls/controls.component';
import { ScoringComponent } from './scoring/scoring.component';
import { HighScoreTableComponent } from './high-score-table/high-score-table.component';

@NgModule({
    imports: [CommonModule, HighScoreModule],
    declarations: [MainMenuComponent, NewGameComponent, HighScoreTableComponent, ControlsComponent, ScoringComponent]
})
export class MainMenuModule { }
