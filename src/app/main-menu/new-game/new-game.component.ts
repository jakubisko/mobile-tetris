import { Component, Output, EventEmitter } from '@angular/core';
import { ScoreService } from '../../game/score.service';
import { GameController } from 'src/app/game/game-controller';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html'
})
export class NewGameComponent {

  @Output()
  public closeMenu = new EventEmitter();

  levelTable: number[];

  constructor(scoreService: ScoreService, private gameController: GameController) {
    this.levelTable = Array.from(Array(scoreService.maxLevel), (_, i) => i + 1);
  }

  newGame(startingLevel: number) {
    this.closeMenu.emit();
    this.gameController.startGame(startingLevel);
  }

}
