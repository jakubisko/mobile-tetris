import { Component } from '@angular/core';
import { HighScores } from '../../high-score/high-score';
import { HighScoreService } from '../../high-score/high-score.service';

@Component({
  selector: 'app-high-score-table',
  templateUrl: './high-score-table.component.html'
})
export class HighScoreTableComponent {

  highScores: HighScores;

  public constructor(private highScoreService: HighScoreService) {
    this.highScores = highScoreService.getHighScores();
  }

  resetHighScores() {
    this.highScoreService.resetHighScores();
    this.highScores = this.highScoreService.getHighScores();
  }

}
