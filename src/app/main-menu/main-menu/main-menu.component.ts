import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HighScoreService } from '../../high-score/high-score.service';

export enum SubPage {
  MainMenu = 'Main menu',
  NewGame = 'Select starting level',
  HighScores = 'High scores',
  Controls = 'Controls',
  Scoring = 'Scoring'
}

@Component({
  templateUrl: './main-menu.component.html',
})
export class MainMenuComponent {

  readonly SubPage = SubPage;
  public subPage = SubPage.MainMenu; // This can be called from outside to switch to high scores table etc.

  isSavingAvailable: boolean;

  public constructor(private activeModal: NgbActiveModal, highScoreService: HighScoreService) {
    this.isSavingAvailable = highScoreService.isSavingAvailable();
  }

  closeMenu() {
    this.activeModal.dismiss();
  }

}
