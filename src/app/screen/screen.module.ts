import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameAreaComponent } from './game-area/game-area.component';
import { MainMenuModule } from '../main-menu/main-menu.module';

@NgModule({
  imports: [CommonModule, MainMenuModule],
  exports: [GameAreaComponent],
  declarations: [GameAreaComponent]
})
export class ScreenModule { }
