import { LINES, COLUMNS } from '../../game/game.module';
import { Component, ElementRef, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { WebGLRenderer, Scene, OrthographicCamera } from 'three';
import { MainMenuComponent } from '../../main-menu/main-menu/main-menu.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TexturePool } from '../../game/texture-pool';
import { GameController } from '../../game/game-controller';
import TWEEN from '@tweenjs/tween.js';

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.css']
})
export class GameAreaComponent implements AfterViewInit {

  @ViewChild('rendererContainer') rendererContainer: ElementRef;

  private renderer: WebGLRenderer;
  private scene: Scene;
  private camera: OrthographicCamera;

  /**
   * Variables used for calculating touch control.
   *
   * Pan gesture is recognized when a pointer is down and moved within a set direction. It is triggered continuously during move.
   * What we want is to move shapes on screen by exactly the pan distance, as if user was dragging the shape.
   *
   * For example, if box size is 25px and user makes a pan gesture of distance 110px, shape should move 4 boxes. In the next pan event, we
   * can ignore 100px as these were already executed.
   */
  private boxSizePx: number; // actual displayed size of one box in pixels, used in mouse event handlers
  private lastPanCommand: string;
  private panDistIgnore = 0;
  private allowHardDrop = true; // hard drop is not allowed during horizontal pan

  public constructor(private gameController: GameController, private modalService: NgbModal, private texturePool: TexturePool) {
    this.init();
  }

  private init() {
    // Camera arguments will be immediately overwritten in manually called onWindowsResize().
    this.camera = new OrthographicCamera(0, 1, 0, 1, 1, 100);
    this.camera.position.set((COLUMNS - 1) / 2, (LINES - 1) / 2 + 2, 50);

    this.scene = new Scene();
    this.gameController.init(this.scene, this.texturePool);
  }

  public ngAfterViewInit() {
    this.renderer = new WebGLRenderer({ antialias: true });
    this.onWindowResize();
    this.rendererContainer.nativeElement.appendChild(this.renderer.domElement);

    const animate = (time: number) => {
      // requestAnimationFrame() is called at rate based on screen refresh rate. Do not base program timing on that.
      TWEEN.update(time);
      requestAnimationFrame(animate);
      this.renderer.render(this.scene, this.camera);
    };

    animate(0);

    this.gameController.startGame(1);
  }

  @HostListener('window:resize')
  public onWindowResize() {
    const rendererElement = this.rendererContainer.nativeElement;
    const width = Math.max(rendererElement.offsetWidth as number, 1);
    const height = Math.max(rendererElement.offsetHeight as number, 1);

    const squaresDisplayed = 1.1 * Math.max(LINES + 3, COLUMNS);
    const aspect = width / height;
    this.camera.left = - squaresDisplayed * aspect / 2;
    this.camera.right = squaresDisplayed * aspect / 2;
    this.camera.top = squaresDisplayed / 2;
    this.camera.bottom = - squaresDisplayed / 2;
    this.camera.updateProjectionMatrix();

    this.boxSizePx = height / squaresDisplayed;

    this.renderer.setSize(width, height);
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case ' ': case 'Enter': case 'ArrowUp': case '8': this.gameController.rotate(); break;
      case 'ArrowRight': case '6': this.gameController.right(); break;
      case 'ArrowLeft': case '4': this.gameController.left(); break;
      case 'ArrowDown': case '2': this.gameController.hardDrop();
    }
  }

  onTap() {
    this.gameController.rotate();
  }

  onPanHorizontal(command: string, distance: number) {
    if (this.lastPanCommand !== command) {
      this.lastPanCommand = command;
      this.panDistIgnore = 0;
    }

    const deltaInBoxes = Math.floor((distance - this.panDistIgnore) / this.boxSizePx);
    for (let i = 0; i < deltaInBoxes; i++) {
      this.doHorizontalMove(command);
    }
    this.panDistIgnore += deltaInBoxes * this.boxSizePx;
  }

  private doHorizontalMove(command: string) {
    switch (command) {
      case 'left': this.gameController.left(); break;
      case 'right': this.gameController.right(); break;
    }
    this.allowHardDrop = false;
  }

  onSwipeDown() {
    if (this.allowHardDrop) {
      this.gameController.hardDrop();
    }
  }

  onFinishGesture() {
    this.allowHardDrop = true;
  }

  openMainMenu() {
    this.gameController.pauseGame();
    this.modalService.open(MainMenuComponent, {
      centered: true,
      beforeDismiss: () => { this.gameController.unpauseGame(); return true; }
    });
  }

}
