import { BrowserModule, HammerModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { ScreenModule } from './screen/screen.module';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
  overrides = {
    // See hammer.js for constants.
    // Horizontal pan is enabled by default. Swipe down has to be enabled manually.
    'swipe': { direction: 16 /* DIRECTION_DOWN */ }
  };
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, HammerModule, NgbModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    ScreenModule
  ],
  bootstrap: [AppComponent],
  providers: [{
    provide: HAMMER_GESTURE_CONFIG,
    useClass: MyHammerConfig
  }]
})
export class AppModule { }
