import { SpriteMaterial, Sprite } from 'three';
import { LINES } from './game.module';

export class Offset {
  constructor(public readonly column: number, public readonly line: number) { }
}

class TetrisShape {
  constructor(public readonly color: string, public readonly boxOffsets: Offset[][]) { }
  public material: SpriteMaterial;
}

/** Defines how to draw and rotate each shape. */
export const shapes: TetrisShape[] = [
  new TetrisShape('#EF7508', [ // I
    [new Offset(-1, 0), new Offset(0, 0), new Offset(1, 0), new Offset(2, 0)],
    [new Offset(0, -2), new Offset(0, -1), new Offset(0, 0), new Offset(0, 1)]
  ]),
  new TetrisShape('#E943FF', [ // J
    [new Offset(-1, 0), new Offset(0, 0), new Offset(1, 0), new Offset(1, -1)],
    [new Offset(0, 1), new Offset(0, 0), new Offset(0, -1), new Offset(-1, -1)],
    [new Offset(1, 0), new Offset(0, 0), new Offset(-1, 0), new Offset(-1, 1)],
    [new Offset(0, -1), new Offset(0, 0), new Offset(0, 1), new Offset(1, 1)]
  ]),
  new TetrisShape('#26F', [ // L
    [new Offset(1, 0), new Offset(0, 0), new Offset(-1, 0), new Offset(-1, -1)],
    [new Offset(0, -1), new Offset(0, 0), new Offset(0, 1), new Offset(-1, 1)],
    [new Offset(-1, 0), new Offset(0, 0), new Offset(1, 0), new Offset(1, 1)],
    [new Offset(0, 1), new Offset(0, 0), new Offset(0, -1), new Offset(1, -1)]
  ]),
  new TetrisShape('red', [ // O
    [new Offset(0, 0), new Offset(0, -1), new Offset(1, 0), new Offset(1, -1)]
  ]),
  new TetrisShape('#8CE4FF', [ // S
    [new Offset(1, 0), new Offset(0, 0), new Offset(0, -1), new Offset(-1, -1)],
    [new Offset(1, -1), new Offset(1, 0), new Offset(0, 0), new Offset(0, 1)]
  ]),
  new TetrisShape('yellow', [ // T
    [new Offset(-1, 0), new Offset(0, 0), new Offset(1, 0), new Offset(0, -1)],
    [new Offset(0, 1), new Offset(0, 0), new Offset(0, -1), new Offset(-1, 0)],
    [new Offset(-1, 0), new Offset(0, 0), new Offset(1, 0), new Offset(0, 1)],
    [new Offset(0, 1), new Offset(0, 0), new Offset(0, -1), new Offset(1, 0)]
  ]),
  new TetrisShape('#5F5', [ // Z
    [new Offset(-1, 0), new Offset(0, 0), new Offset(0, -1), new Offset(1, -1)],
    [new Offset(0, -1), new Offset(0, 0), new Offset(1, 0), new Offset(1, 1)]
  ])
];

/** Shape located on screen. Use it to draw "moving shape" or "next shape". */
export class PositionedShape {

  public constructor(public shape: TetrisShape) { }

  public boxes: Sprite[] = [];
  public line: number;
  public column: number;
  public orientation = 0;

  public updateBoxes() {
    const boxOffsets = this.shape.boxOffsets[this.orientation];
    for (let i = 0; i < this.boxes.length; i++) {
      const column = this.column + boxOffsets[i].column;
      const line = this.line + boxOffsets[i].line;
      const hidden = line === LINES; // This hides boxes of moving shape that are overflowing the top of the grid.
      this.boxes[i].position.set(column, line, hidden ? -1000 : 0);
    }
  }

}
