import { LINES, COLUMNS } from './game.module';
import { Scene, RepeatWrapping, Sprite, SpriteMaterial } from 'three';
import { Text2D, TextOptions } from 'three-text2d/lib/Text2D';
import { textAlign, SpriteText2D } from 'three-text2d';
import { ScoreService } from './score.service';
import { PositionedShape, shapes } from './shapes';
import { TexturePool } from './texture-pool';

/** Handles drawing of things around the grid - bondary, score, next shape. Mostly static elements. */
export class GameBoundary {

  private readonly TEXT_OPTIONS: TextOptions = { align: textAlign.center, font: 'bold 100px Lucida Console', fillStyle: 'rosybrown' };

  private scoreText: Text2D;
  private levelText: Text2D;

  private nextShapes: PositionedShape[] = [];

  public constructor(private scene: Scene, private scoreService: ScoreService, texturePool: TexturePool) {
    this.initBackground(texturePool);
    this.initBoundary(texturePool);
    this.initLabels();
  }

  private initBackground(texturePool: TexturePool) {
    const bricksTexture = texturePool.getTexture('bricks');
    bricksTexture.wrapS = RepeatWrapping;
    bricksTexture.wrapT = RepeatWrapping;
    bricksTexture.repeat.set(30, 30);

    const material = new SpriteMaterial({ map: bricksTexture, color: 'rosybrown' });
    const sprite = new Sprite(material);
    sprite.scale.set(100, 100, 1)
    sprite.position.z = -2;
    this.scene.add(sprite);
  }

  private initBoundary(texturePool: TexturePool) {
    const backTexture = texturePool.getTexture('back');
    const backMaterial = new SpriteMaterial({ map: backTexture });
    backTexture.wrapS = RepeatWrapping;
    backTexture.wrapT = RepeatWrapping;
    backTexture.repeat.set(2, 4);

    const addRectangle = (width: number, height: number, x: number, y: number) => {
      const sprite = new Sprite(backMaterial);
      sprite.scale.set(width, height, 1)
      sprite.position.set(x, y, -1)
      this.scene.add(sprite);
    };

    addRectangle(COLUMNS, LINES, (COLUMNS - 1) / 2, (LINES - 1) / 2); // Grid
    addRectangle(4.75, 2.75, (COLUMNS - 1) / 2, LINES + 2); // Next shape; Box holding biggest shape is 4 x 2.
    addRectangle(4.5, 2.75, (COLUMNS - 1) / 2 - 5, LINES + 2); // Score
    addRectangle(4.5, 2.75, (COLUMNS - 1) / 2 + 5, LINES + 2); // Level
  }


  private initLabels() {
    const scoreLabel = new SpriteText2D('Score', this.TEXT_OPTIONS);
    scoreLabel.scale.set(0.01, 0.01, 1);
    scoreLabel.position.set((COLUMNS - 1) / 2 - 5, LINES + 2.5, 1);
    this.scene.add(scoreLabel);

    const levelLabel = new SpriteText2D('Level', this.TEXT_OPTIONS);
    levelLabel.scale.set(0.01, 0.01, 1);
    levelLabel.position.set((COLUMNS - 1) / 2 + 5, LINES + 2.5, 1);
    this.scene.add(levelLabel);
  }

  public updateScoreText() {
    if (this.scoreText != null) {
      this.scene.remove(this.scoreText);
    }
    this.scoreText = new SpriteText2D('' + this.scoreService.score, this.TEXT_OPTIONS);
    this.scoreText.scale.set(0.01, 0.01, 1);
    this.scoreText.position.set((COLUMNS - 1) / 2 - 5, LINES + 1.5, 1);
    this.scene.add(this.scoreText);

    if (this.levelText != null) {
      this.scene.remove(this.levelText);
    }
    this.levelText = new SpriteText2D('' + this.scoreService.level, this.TEXT_OPTIONS);
    this.levelText.scale.set(0.01, 0.01, 1);
    this.levelText.position.set((COLUMNS - 1) / 2 + 5, LINES + 1.5, 1);
    this.scene.add(this.levelText);
  }

  /** Randomly shuffles given array in place. Returns the reference to the aray. */
  private shuffle<T>(a: T[]): T[] {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor((i + 1) * Math.random());
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }


  /** Creates a new random shape and displays it as next shape. Returns previous next shape. */
  public advanceNextShape(): PositionedShape {
    if (this.nextShapes.length <= 1) {
      this.nextShapes = [
        ...this.nextShapes,
        ...this.shuffle(shapes.map(shape => new PositionedShape(shape)))
      ];
    }

    const shapeToDrop = this.nextShapes.shift();
    const shapeToDisplay = this.nextShapes[0];

    shapeToDisplay.column = COLUMNS / 2 - 1;
    shapeToDisplay.line = LINES + 2.5;
    shapeToDisplay.boxes = shapeToDisplay.shape.boxOffsets[shapeToDisplay.orientation]
      .map(_ => new Sprite(shapeToDisplay.shape.material));
    shapeToDisplay.boxes.forEach(box => this.scene.add(box));
    shapeToDisplay.updateBoxes();

    return shapeToDrop;
  }

}
