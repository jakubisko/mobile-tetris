import { Scene, Sprite } from 'three';
import TWEEN from '@tweenjs/tween.js';

/** Makes the animation for cleared lines using sprites. */
export class LineClearAnimation {

  public constructor(private scene: Scene) { }

  private random(from: number, to: number): number {
    return to + Math.random() * (from - to);
  }

  private setBoxToMotion(box: Sprite) {
    box.position.z = 1;

    const duration = this.random(400, 800);

    new TWEEN.Tween({ x: box.position.x, y: box.position.y })
      .easing(TWEEN.Easing.Circular.Out)
      .to({ x: box.position.x + this.random(-9, 9), y: box.position.y + this.random(1.2, -1.2) }, duration)
      .onUpdate(({ x, y }) => {
        box.position.x = x;
        box.position.y = y;
      })
      .start(undefined);

    new TWEEN.Tween({ x: 1, y: 1 })
      .to({ x: 0, y: 0 }, duration)
      .onUpdate(({ x, y }) => {
        box.scale.x = x;
        box.scale.y = y;
      })
      .start(undefined);

    new TWEEN.Tween({})
      .to({}, duration + 1000)
      .onComplete(() => this.scene.remove(box))
      .start(undefined);
  }

  /** Scatters the boxes from the given line. At the end of the animation, removes the boxes from the scene. */
  public scatterBoxes(boxes: Sprite[]) {
    for (const box of boxes) {
      if (box != null) {
        this.setBoxToMotion(box);
      }
    }
  }

}
