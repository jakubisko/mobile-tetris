import { ScoreService } from './score.service';
import { Scene } from 'three';
import { GameBoundary } from './game-boundary';
import { TexturePool } from './texture-pool';
import { GameGrid } from './game-grid';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HighScoreService } from '../high-score/high-score.service';
import { EnterNameComponent } from '../high-score/enter-name/enter-name.component';
import { MainMenuComponent, SubPage } from '../main-menu/main-menu/main-menu.component';

/** Whether game has started, is lost... */
enum OverallState {
  Initializing, // Before game, while loading textures
  Playing,
  Paused, // Only a game that is in state "Playing" can be paused. Lost game can't be paused.
  Lost
}

/** Use this to input game commands. */
@Injectable({
  providedIn: 'root'
})
export class GameController {

  private overallState = OverallState.Initializing;
  private gravityTimer: NodeJS.Timer;

  private gameBoundary: GameBoundary;
  private gameGrid: GameGrid;

  public constructor(private scoreService: ScoreService, private modalService: NgbModal, private highScoreService: HighScoreService) { }

  public init(scene: Scene, texturePool: TexturePool) {
    this.gameBoundary = new GameBoundary(scene, this.scoreService, texturePool);
    this.gameGrid = new GameGrid(scene, this.scoreService, texturePool, this.gameBoundary, this);
  }

  public startGame(startingLevel: number) {
    this.scoreService.resetScore(startingLevel);
    this.gameBoundary.updateScoreText();
    this.gameGrid.cleanGrid();

    this.startGravity();
    this.overallState = OverallState.Playing;
  }

  public endGame() {
    this.stopGravity();
    this.overallState = OverallState.Lost;

    const score = this.scoreService.score;
    if (this.highScoreService.isSavingAvailable() && this.highScoreService.eligibleForHighScore(score)) {
      EnterNameComponent.openModal(this.modalService, score, (name) => {
        this.highScoreService.addHighScore(name, score);
        const menu = this.modalService.open(MainMenuComponent, { centered: true }).componentInstance as MainMenuComponent;
        menu.subPage = SubPage.HighScores;
      });
    }
  }

  /** If the game is running, pauses the game. Does nothing in any other state. */
  public pauseGame() {
    if (this.overallState === OverallState.Playing) {
      this.stopGravity();
      this.overallState = OverallState.Paused;
    }
  }

  public unpauseGame() {
    if (this.overallState === OverallState.Paused) {
      this.startGravity();
      this.overallState = OverallState.Playing;
    }
  }

  private startGravity() {
    if (this.gravityTimer == null) {
      const tick = () => {
        this.gravityTimer = setTimeout(tick, this.scoreService.speed);
        this.down();
      };
      this.gravityTimer = setTimeout(tick, this.scoreService.speed);
    }
  }

  private stopGravity() {
    if (this.gravityTimer != null) {
      clearTimeout(this.gravityTimer);
      this.gravityTimer = null;
    }
  }

  public left() {
    if (this.overallState === OverallState.Playing) {
      this.gameGrid.left();
    }
  }

  public right() {
    if (this.overallState === OverallState.Playing) {
      this.gameGrid.right();
    }
  }

  public down() {
    if (this.overallState === OverallState.Playing) {
      this.gameGrid.down();
    }
  }

  public hardDrop() {
    if (this.overallState === OverallState.Playing) {
      this.gameGrid.hardDrop();
    }
  }

  public rotate() {
    if (this.overallState === OverallState.Playing) {
      this.gameGrid.rotate();
    }
  }

}
