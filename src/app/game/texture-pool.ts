import { Texture, TextureLoader } from 'three';
import { Injectable } from '@angular/core';

/** Loads multiple textures before first drawing, so it can then return textures without delay. */
@Injectable({
  providedIn: 'root'
})
export class TexturePool {

  private readonly RESOURCES = [
    ['box', '/assets/images/box.svg'],
    ['back', '/assets/images/back.jpg'],
    ['bricks', '/assets/images/bricks.jpg'],
  ];

  private urlToTexture: Map<string, Texture> = new Map<string, Texture>();

  public preloadTextures(onLoad: () => void) {
    const textureLoader = new TextureLoader();
    let remains = this.RESOURCES.length;

    for (const resource of this.RESOURCES) {
      const name = resource[0];
      const url = resource[1];

      textureLoader.load(url, (texture) => {
        this.urlToTexture.set(name, texture);
        if (--remains === 0) {
          onLoad();
        }
      });
    }
  }

  /** Returns texture with given name. Unlike TextureLoader.load(), this returns immediately as textures are pre-loaded. */
  public getTexture(name: string): Texture {
    const texture = this.urlToTexture.get(name);
    if (texture == null) {
      throw new Error('Texture with name "' + name + '" was not preloaded by TexturePool!');
    }
    return texture;
  }

}
