import { LINES, COLUMNS } from './game.module';
import { ScoreService } from './score.service';
import { shapes, PositionedShape } from './shapes';
import { Sprite, Texture, Scene, SpriteMaterial } from 'three';
import { GameBoundary } from './game-boundary';
import { TexturePool } from './texture-pool';
import { GameController } from './game-controller';
import { LineClearAnimation } from './line-clear-animator';

/** Holds static boxes and moving shape. Checks for collisions. */
export class GameGrid {

  private grid: Sprite[][]; // [line][column]; this enables quick removal of entire line
  private movingShape: PositionedShape;
  private lineClearAnimator: LineClearAnimation;

  public constructor(private scene: Scene, private scoreService: ScoreService, texturePool: TexturePool,
    private gameBoundary: GameBoundary, private gameController: GameController) {
    this.initGrid();

    const boxTexture = texturePool.getTexture('box');
    this.initMaterials(boxTexture);

    this.lineClearAnimator = new LineClearAnimation(scene);
  }

  private initMaterials(boxTexture: Texture) {
    shapes.forEach(shape => shape.material = new SpriteMaterial({ map: boxTexture, color: shape.color }));
  }

  private initGrid() {
    this.grid = new Array(LINES);
    for (let line = 0; line < LINES; line++) {
      const newLine = new Array<Sprite>(COLUMNS);
      for (let column = 0; column < COLUMNS; column++) {
        newLine[column] = null;
      }
      this.grid[line] = newLine;
    }
  }

  /** Prepares for next game by cleaning all objects. Unlike init, this is called before each game. */
  public cleanGrid() {
    // Removes static boxes from the scene, if there are any left from previous game
    for (let line = 0; line < LINES; line++) {
      for (let column = 0; column < COLUMNS; column++) {
        if (this.grid[line][column] != null) {
          this.scene.remove(this.grid[line][column]);
          this.grid[line][column] = null;
        }
      }
    }

    // Removes moving shape from Scene, if there is any left from previous game
    if (this.movingShape != null) {
      this.movingShape.boxes.forEach(box => this.scene.remove(box));
    }

    // Removes previous next shape from Scene, if there is any left from previous game
    // Generates a new next shape
    const nextShape = this.gameBoundary.advanceNextShape();
    if (nextShape != null) {
      nextShape.boxes.forEach(box => this.scene.remove(box));
    }

    // Starts first moving shape of the game
    this.nextShapeToMovingShape();
  }

  /** Takes next shape and places it as moving shape at the top of the grid. If it colides with static grid, game ends. */
  private nextShapeToMovingShape() {
    this.movingShape = this.gameBoundary.advanceNextShape();
    this.movingShape.column = COLUMNS / 2 - 1;
    this.movingShape.line = LINES - 1;
    this.movingShape.updateBoxes();

    if (!this.canBePlacedAt(this.movingShape.column, this.movingShape.line, this.movingShape.orientation)) {
      this.gameController.endGame();
    }
  }

  /**
   * Checks whether it is possible to place the moving shape at the given column and line in given orientation.
   * Rotation of shape on top boundary may result in box being above the top boundary; that is allowed.
   * Returns false if the shape overlaps boundary or some static box. Returns true otherwise.
   */
  private canBePlacedAt(newColumn: number, newLine: number, newOrientation: number): boolean {
    const boxOffsets = this.movingShape.shape.boxOffsets[newOrientation];
    for (const offset of boxOffsets) {
      const line = newLine + offset.line;
      const column = newColumn + offset.column;
      if (column < 0 || line < 0 || column >= COLUMNS || (line < LINES && this.grid[line][column] != null)) {
        return false;
      }
    }

    return true;
  }

  /** Makes the boxes of the moving shape part of the static grid. */
  private makeStatic() {
    const boxOffsets = this.movingShape.shape.boxOffsets[this.movingShape.orientation];
    for (let i = 0; i < this.movingShape.boxes.length; i++) {
      const column = this.movingShape.column + boxOffsets[i].column;
      const line = this.movingShape.line + boxOffsets[i].line;
      this.grid[line][column] = this.movingShape.boxes[i];
    }
  }

  private clearCompletedLines() {
    let linesCleared = 0;

    let lineChecked = 0;
    while (lineChecked < LINES - linesCleared) {
      if (this.grid[lineChecked].includes(null)) {
        lineChecked++;
      } else {
        // Make line clear animation
        this.lineClearAnimator.scatterBoxes(this.grid[lineChecked]);

        // Remove line
        this.grid.splice(lineChecked, 1);
        linesCleared++;

        // Add new line
        const newLine = new Array<Sprite>(COLUMNS);
        for (let column = 0; column < COLUMNS; column++) {
          newLine[column] = null;
        }
        this.grid.push(newLine);
      }
    }

    // If some lines were cleared, move all boxes on screen to their new position
    if (linesCleared > 0) {
      for (let line = 0; line < LINES - linesCleared - 1; line++) {
        for (let column = 0; column < COLUMNS; column++) {
          const box = this.grid[line][column];
          if (box !== null) {
            box.position.set(column, line, 0);
          }
        }
      }
    }

    this.scoreService.linesCleared(linesCleared);
    this.gameBoundary.updateScoreText();
  }

  public left() {
    if (this.canBePlacedAt(this.movingShape.column - 1, this.movingShape.line, this.movingShape.orientation)) {
      this.movingShape.column--;
      this.movingShape.updateBoxes();
    }
  }

  public right() {
    if (this.canBePlacedAt(this.movingShape.column + 1, this.movingShape.line, this.movingShape.orientation)) {
      this.movingShape.column++;
      this.movingShape.updateBoxes();
    }
  }

  public down() {
    if (this.canBePlacedAt(this.movingShape.column, this.movingShape.line - 1, this.movingShape.orientation)) {
      this.movingShape.line--;
      this.movingShape.updateBoxes();
    } else {
      this.makeStatic();
      this.clearCompletedLines();
      this.nextShapeToMovingShape();
    }
  }

  /** Instantly moves shape down until it hits obstacle. Unlike down, this awards extra points. */
  public hardDrop() {
    const originalLine = this.movingShape.line;
    while (this.canBePlacedAt(this.movingShape.column, this.movingShape.line - 1, this.movingShape.orientation)) {
      this.movingShape.line--;
    }

    this.scoreService.hardDrop(originalLine - this.movingShape.line);
    this.gameBoundary.updateScoreText();

    this.movingShape.updateBoxes();
    this.down(); // Final movement to make the shape static
  }

  public rotate() {
    const newOrientation = (this.movingShape.orientation + 1) % this.movingShape.shape.boxOffsets.length;
    if (this.canBePlacedAt(this.movingShape.column, this.movingShape.line, newOrientation)) {
      this.movingShape.orientation = newOrientation;
      this.movingShape.updateBoxes();
    }
  }

}
