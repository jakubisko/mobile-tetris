import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private readonly linesPerLevel = 10;
  private readonly scoreTable = [100, 300, 500, 800];
  private readonly speedTable = [0, 1000, 800, 640, 512, 410, 328, 262, 209, 168, 134, 107, 86, 69, 55, 44];

  private _score: number;
  private _level: number;
  private lines: number;
  private combos: number;

  public get score() { return this._score; }
  public get level() { return this._level; }
  public get maxLevel() { return this.speedTable.length; }

  public get speed() {
    return this._level >= this.speedTable.length ? this.speedTable[this.speedTable.length - 1] : this.speedTable[this._level];
  }

  public constructor() {
    this.resetScore(1);
  }

  /** Resets score, starts from given level. First level is 1. */
  public resetScore(startingLevel: number) {
    this._score = 0;
    this._level = startingLevel;
    this.lines = this.linesPerLevel * (startingLevel - 1);
    this.combos = 0;
  }

  /** Call this whenever a shape is made static, even when 0 lines are cleared. This is to reset combo counter. */
  public linesCleared(noOfLinesCleared: number) {
    if (noOfLinesCleared > 0) {
      this._score += (this.scoreTable[noOfLinesCleared - 1] + 50 * this.combos++) * this._level;
      this.lines += noOfLinesCleared;
      this._level = 1 + Math.floor(this.lines / this.linesPerLevel);
    } else {
      this.combos = 0;
    }
  }

  public hardDrop(noOfLinesTravelled: number) {
    this._score += 2 * noOfLinesTravelled * this._level;
  }

}
