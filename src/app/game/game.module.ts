import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Size of the field. */
export const LINES = 20;
export const COLUMNS = 10;

@NgModule({
  imports: [CommonModule],
  declarations: []
})
export class GameModule { }
